import os
import json
import pytest
from fixture.application import Application
import jsonpickle

fixture = None
target = None


def load_config(file):
    global target
    if target is None:
        config_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), file)
        with open(config_file) as f:
            target = json.load(f)
    return target


@pytest.fixture()
def app(request):
    global fixture
    browser = request.config.getoption("--browser")
    web_config = load_config(request.config.getoption("--target"))['web']
    if fixture is None or not fixture.is_valid():
        fixture = Application(browser=browser, base_url=web_config['baseUrl'])
    fixture.session.login(username=web_config['username'], password=web_config['password'])
    return fixture


@pytest.fixture(autouse=True)
def stop(request):
    def fin():
        fixture.session.logout()
        fixture.destroy()

    request.addfinalizer(fin)
    return fixture


def pytest_addoption(parser):
    parser.addoption("--browser", action="store", default="chrome")
    parser.addoption("--target", action="store", default="target.json")


def pytest_generate_tests(metafunc):
    if "json_campaigns" in metafunc.fixturenames:
        testdata = load_from_json("campaigns")
        metafunc.parametrize("json_campaigns", testdata)


def load_from_json(file):
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "data/{}.json".format(file)),
                           mode="r", encoding="utf-8") as f:
        return jsonpickle.decode(f.read())
