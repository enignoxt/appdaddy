def test_create_ad_company(app, json_campaigns):
    campaign = json_campaigns
    # выброр "Персональная кампания"
    app.campaign.select_campaign_type(campaign.campaign_type)
    # поиск блоггера "HiMan"
    app.campaign.find_blogger_by_name(campaign.blogger_name)
    # выбор тип привлечения = видео
    app.campaign.select_promotion_type(campaign.promotion_type)
    # сохранение цены выбронного типа привличения
    price = app.campaign.save_selected_price()
    # подтверждение
    app.campaign.confirm_promotion_type()
    # провка, что на открывшейся странице есть только один блоггер и цена совпадает с выбранной
    assert app.campaign.get_bloggers_count_on_confirm_page() == 1
    assert app.campaign.get_result_price() == price
