from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class SessionHelper:
    def __init__(self, app):
        self.app = app

    def login(self, username, password):
        wd = self.app.wd
        wait = WebDriverWait(wd, 10)
        self.app.open_welcome_page()
        # "Вход"
        wd.find_element_by_xpath("/ html / body / header / section / nav / ul / li[4] / a").click()
        # email
        wd.find_element_by_id("appdaddy_login_email").clear()
        wd.find_element_by_id("appdaddy_login_email").send_keys(username)
        # пароль
        wd.find_element_by_id("appdaddy_login_password").clear()
        wd.find_element_by_id("appdaddy_login_password").send_keys(password)
        # "Войти"
        wd.find_element_by_id("jsSigninBtn").click()

        # ждем полной загрузки главной страницы
        # надпись "Вовлечения"
        wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".tabs.ng-scope.active")))

    def logout(self):
        wd = self.app.wd
        wd.find_element_by_xpath("//a[@class='b__head-user action-drop-menu']").click()
        wd.find_element_by_xpath("//nav[@class='head-nav_dropdown-list']/a[@href='/logout']").click()



