from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import re


class CampaignHelper:
    def __init__(self, app):
        self.app = app

    def select_campaign_type(self, campaign_type):
        wd = self.app.wd
        wait = WebDriverWait(wd, 10)
        # "Создать"
        wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, ".b_button_link.ng-isolate-scope"))).click()
        # выбор типа компании
        wait.until(EC.element_to_be_clickable((By.XPATH, "//ul[@class='campaign-type']"
                                                         "/li/label/div[text() = '" + campaign_type + "']")))

        # "Продолжить"
        wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,
                                               ".new-design.button.button--font-sm-WC."
                                               "button--padding-md.button--right-arrow"))).click()
        # ждем полной загрузки страницы
        wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, ".all-found-bloggers.ng-scope")))

    def find_blogger_by_name(self, name):
        wd = self.app.wd
        wait = WebDriverWait(wd, 10)
        # ввод имени в поле "Поиск блоггеров"
        input_name = wd.find_element_by_xpath("//*[@id='formly_1_textSearch_text_search_0']/input")
        input_name.click()
        input_name.send_keys(name + Keys.ENTER)
        # ждем загрузки результатов поиска
        wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='new-design font-body-2-sc']")))

    def select_promotion_type(self, promotion_type):
        wd = self.app.wd
        # кнопка окна выбора типа привлечения
        wd.find_element_by_class_name("multi-price-btn").click()
        # выбор типа привлечения
        button = wd.find_element_by_xpath("//ul[@class='multi-price-btn-dropdown']"
                                          "/li/div/div[contains(text(),'" + promotion_type + "')]/../../div/button")
        button.click()

    def save_selected_price(self):
        wd = self.app.wd
        price = wd.find_element_by_xpath("//div[@class='new-design button--font-sm-WC ng-binding ng-scope']").text
        price = re.sub("[^0-9]", "", price)
        return price

    def confirm_promotion_type(self):
        wd = self.app.wd
        wait = WebDriverWait(wd, 10)
        # кнопка подтверждения
        wd.find_element_by_xpath("//div[@class='footer-result']/button").click()
        wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@class='select-and-view__left']")))

    def get_result_price(self):
        wd = self.app.wd
        price = wd.find_element_by_xpath("//div[@class='footer-result']").text
        price = re.sub("[^0-9]", "", price)
        return price

    def get_bloggers_count_on_confirm_page(self):
        wd = self.app.wd
        count = len(wd.find_elements(By.XPATH, "//ul[@class='basket-blogger-list']/li"))
        return count
