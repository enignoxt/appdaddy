from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

from selenium.webdriver.chrome.options import Options

from fixture.session import SessionHelper
from fixture.campaign import CampaignHelper


class Application:
    def __init__(self, browser, base_url):
        if browser == "chrome":
            chrome_options = Options()
            chrome_options.add_argument("--disable-infobars")
            self.wd = webdriver.Chrome(chrome_options=chrome_options)
        elif browser == "firefox":
            self.wd = webdriver.Firefox()
        else:
            raise ValueError("Unrecognized browser %s" % browser)
        self.wd.set_window_size(1280, 800)

        self.session = SessionHelper(self)
        self.campaign = CampaignHelper(self)

        self.base_url = base_url
        self.wd.implicitly_wait(10)
        self.wait = WebDriverWait(self.wd, 10)

    def open_welcome_page(self):
        wd = self.wd
        wd.get(self.base_url)

    def is_valid(self):
        try:
            self.wd.current_url()
            return True
        except:
            return False

    def destroy(self):
        self.wd.quit()
