class Campaign:
    def __init__(self, campaign_type=None, blogger_name=None, promotion_type=None):
        self.campaign_type = campaign_type
        self.blogger_name = blogger_name
        self.promotion_type = promotion_type

    def __repr__(self):
        return "{}:{}:{}".format(self.campaign_type, self.blogger_name, self.promotion_type)

    def __eq__(self, other):
        return (self.campaign_type == other.campaign_type and
                self.blogger_name == other.blogger_name and
                self.promotion_type == other.promotion_type)
